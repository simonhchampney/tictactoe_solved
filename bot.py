class endtype:
    types={1:"pl1", 2:"pl2", 0:"tie"}
    def __init__(self, num):
        self.self=num

class branch:
    def __init__(self, board, turn):
        self.turn=turn
        self.root=board
        ifend=whos_game(board)
        self.p1s=0
        self.p2s=0
        if(ifend==-1):
            if(self.turn==1):
                self.p1s-=3
            else:
                self.p2s-=3
            self.children=[]
            for child in get_possibles(board):
                if(self.turn==1):
                    bChild=branch(child, 2)
                else:
                    bChild=branch(child, 1)
                self.children.append(bChild)
                self.p1s+=bChild.p1s
                self.p2s+=bChild.p2s
                

                    
        else:
            self.children=endtype(ifend)
            if(self.children.self==1):
                self.p1s+=2
                self.p2s-=2
            if(self.children.self==2):
                self.p2s+=2
                self.p1s-=2
            else:
                self.p1s-=1
                self.p2s-=1

def copyls(board):
    copyofboard=[]
    for y in range(len(board)):
        copyofboard.append(board[y][:])
    return copyofboard

def init_board(x, y):
    board = []
    for y1 in range(y):
        board.append([])
        for x1 in range(x):
            board[y1].append(0)
    return board

def display_board(board, clear=True):
    if(clear):
        print("\033c")
    for y in range(3):
        for x in range(3):
            print(board[y][x], end="")
        print("")
    sleep(0.3)

def best_move(focus, turn):
    scores=[]
    for child in focus.children:
        scores.append(child.p1s-child.p2s)
    scores.sort()
    print(scores)
    score=None
    if(turn==1):
        score=scores[-1]
    else:
        score=scores[0]
    for child in focus.children:
        if(child.p1s-child.p2s==score):
            return child

def spefin(phrase, possibles, error):
    while(True):
        user=input(phrase)
        if(user in possibles):
            return user
        else:
            print(error)