from time import sleep
import pickle, 
from ..bot, tic_tac_toe import *



board=init_board(3,3)
data=None

try:
    with open("data.txt", "rb") as f:
        data=pickle.load(f)
except:
    with open("data.txt", "wb") as f:
        data=branch(board, 1)
        pickle.dump(data, f)

focus = data
if(spefin("What do you want to play as? (1,2)", ["1", "2"], "Must be 1 or 2")=="1"):
    while(True):
        focus=get_player_move(focus)
        display_board(focus.root, 0)
        if(type(focus.children)==endtype):
            break
        focus=best_move(focus, 2)
        display_board(focus.root, 0)
        if(type(focus.children)==endtype):
            break
else:
    focus=best_move(focus, 1)
    display_board(focus.root, 0)
    while(True):
        focus=get_player_move(focus)
        display_board(focus.root, 0)
        if(type(focus.children)==endtype):
            break
        focus=best_move(focus, 1)
        display_board(focus.root, 0)
        if(type(focus.children)==endtype):
            break
print("Game over")



#0: tie, 1: win, 2: cont, -1: lose
#[[brd1, move1, cont], [brd2=move1, move2, lost]]
#[[brd1, move1, lost]]
    
