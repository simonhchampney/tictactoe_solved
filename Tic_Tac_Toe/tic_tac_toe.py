def whos_game(board):
    for y in range(len(board)):
        xs1=[]
        xs2=[]
        ys1=[]
        ys2=[]
        for x in range(len(board[y])):
            xs1.append(board[y][x]==1)
            xs2.append(board[y][x]==2)
            ys1.append(board[x][y]==1)
            ys2.append(board[x][y]==2)
        if(all(xs1) or all(ys1)):
            return 1
        if(all(xs2) or all(ys2)):
            return 2
    for n in range(1, 3):
        if((board[0][0]==n and board[1][1]==n and board[2][2]==n) or (board[2][0]==n and board[1][1]==n and board[0][2]==n)):
            return n
    isall=0
    for row in board:
        for item in row:
            if(item):
                isall+=1
    if(isall==9):
        return 0
    return -1
#pl1: 1, pl2: 2, tie:0, cont:-1

def get_possibles(board):
    pl1=0
    pl2=0
    for y in range(len(board)):
        for x in range(len(board[y])):
            if(board[y][x]==1):
                pl1+=1
            if(board[y][x]==2):
                pl2+=1
    ply=1
    if(pl1>pl2):
        ply=2
    possibles=[]
    for y in range(len(board)):
        for x in range(len(board[y])):
            if(board[y][x]==0):
                tempboard=copyls(board)
                tempboard[y][x]=ply
                possibles.append(tempboard)
    return possibles

def get_player_move(focus):
    options=[]
    num=len(focus.children)
    for i in range(num):
        print(i+1, ".)", sep="")
        options.append(str(i+1))
        display_board(focus.children[i].root, 0)
    return focus.children[int(spefin("What move do you want to make?",
        options, "Must be an option 1-"+str(num)))-1]

